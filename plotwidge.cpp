#include "plotwidge.h"

PlotWidget::PlotWidget(QQuickItem *parent) : QQuickPaintedItem(parent)
{
    fileModel = std::make_unique<FileModel>("/home/slava/PROJECTS/test_exercise/TestTaskEnglish/SampleFiles/MillionSamplesWithTail.ssd");

    auto [data, maxmin] = fileModel->loadData(3000);
    maxmin_ = maxmin;
    for (const auto &pair : data) {
        dataPoints.append(QPointF(pair.first, pair.second));
    }

}

void PlotWidget::paint(QPainter* painter)
{
    painter->setRenderHint(QPainter::Antialiasing, true);

    QPen pen = painter->pen();
    pen.setColor(Qt::blue);
    pen.setWidth(2);
    painter->setPen(pen);
    painter->setBrush(Qt::red);

    double minX = maxmin_.minX, maxX = maxmin_.maxX;
    double minY = maxmin_.minY, maxY = maxmin_.maxY;

    double scaleX = width() / (maxX - minX);
    double scaleY = height() / (maxY - minY);
    qDebug()<< "MAXy:" << maxY << " slaceY:" << scaleY << " height:" << height() << "scale:" << height() / maxY;
    qDebug()<< "scaledY:" << maxY * scaleY;

    QPointF previousPoint;
    bool firstPoint = true;
    for (const QPointF &point : dataPoints) {
        QPointF scaledPoint((point.x() - minX) * scaleX, height() - (point.y() - minY) * scaleY);
        if (!firstPoint) {
            painter->drawLine(previousPoint, scaledPoint);
        }
        previousPoint = scaledPoint;
        firstPoint = false;

        painter->drawEllipse(scaledPoint, 3, 3);
    }
}
