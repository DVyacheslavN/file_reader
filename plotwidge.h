#ifndef PLOTWIDGE_H
#define PLOTWIDGE_H

#include <QWidget>
#include <QPainter>
#include <QDateTime>
#include <QQuickPaintedItem>
#include "FileModel/FileModel.h"

class PlotWidget : public QQuickPaintedItem
{
    Q_OBJECT

public:
    explicit PlotWidget(QQuickItem *parent = nullptr);

protected:
    void paint(QPainter* painter) override;

private:
    QVector<QPointF> dataPoints;
    std::unique_ptr<FileModel> fileModel;
    MaxMin maxmin_;
};


#endif // PLOTWIDGE_H
