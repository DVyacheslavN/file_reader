/*
   Copyright (C) 2024 Viacheslav Domnin

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/


#include "FileParser.h"
#include <iostream>

#include <string>
#include <tuple>
#include <vector>
#include <list>
#include <fcntl.h>
#include <unistd.h>
#include <boost/interprocess/file_mapping.hpp>
#include <boost/interprocess/mapped_region.hpp>

std::pair<std::string, std::size_t> FileParser::getLineAndPosition(bool reset)
{
    std::size_t pos = lineStart - first;
    std::string line = reset ? getLine(0) : getLine();
    return {line, pos};
}

void FileParser::reset()
{
    first = static_cast<char *>(region->get_address());
    size_t size = region->get_size();
    end = first + size;
    lineStart = first;
}

char * FileParser::begin() {
    return first;
}

char * FileParser::last() {
    return end;
}


std::string FileParser::getLine(std::size_t byteShift)
{
    if (first == nullptr || end == nullptr || lineStart == nullptr)
    {
        reset();
    }

    if (byteShift != std::string::npos)
    {
        char *newPosition = nullptr;
        newPosition = first + byteShift;
        if (newPosition < end)
        {
            while ((*newPosition == '\n' || *newPosition == '\r') && newPosition < end)
            {
                newPosition++;
            }

            if (newPosition < end && newPosition != first && (*(newPosition - 1) != '\n' || *(newPosition - 1) != '\r'))
            {
                newPosition = static_cast<char *>(memchr(newPosition, '\n', end - newPosition));
            }
            lineStart = newPosition < end ? newPosition : nullptr;
        }
    }

    if (lineStart < end && lineStart != nullptr)
    {
        while ((*lineStart == '\n' || *lineStart == '\r') && lineStart < end)
        {
            lineStart++;
        }
        // std::cout << "size: " << end - lineStart << "pointer: " << lineStart << std::endl;
        char *lineEnd = static_cast<char *>(memchr(lineStart, '\n', end - lineStart));
        if (lineEnd != nullptr)
        {
            // lineStart = *lineStart == '\r' ? lineStart + 1 : lineStart;
            auto buff = lineStart;
            lineStart = lineEnd + 1;
            return (std::string(buff, lineEnd));
        }
        else
        {
            return "";
        }
    }
    return "";
}

std::size_t FileParser::size()
{
    return region->get_size();
}

std::size_t FileParser::curret()
{
    return lineStart - first;
}

FileParser::FileParser(std::string filename) : filename_(filename)
{
    fileMap = std::make_unique<file_mapping>(filename_.c_str(), read_write);
    region = std::make_unique<mapped_region>(*fileMap, read_write);
}

FileParser::~FileParser()
{
    region.reset();
    fileMap.reset();
}
