/*
   Copyright (C) 2024 Viacheslav Domnin

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

#ifndef _FILE_PARSER_HELPER_H_
#define _FILE_PARSER_HELPER_H_

// #include "range/v3/all.hpp"
// #include "range/v3/view.hpp"
// using zip_t = std::ranges::views::zip;
#include <algorithm>
#include <execution>
#include <sstream>
#include <iostream>
#include <regex>
#include <ctime>
#include <iomanip>
// #include <format>
#include <chrono>

namespace fp
{

    // template <class T>
    // inline auto zip(T &array)
    // {
    //     auto indices = ranges::views::iota(0ul, array.size());
    //     auto zipped = ranges::views::zip(indices, array);
    //     return zipped;
    // }

    // inline std::string trim(const std::string &str)
    // {
    //     size_t first = str.find_first_not_of(" \t");
    //     size_t last = str.find_last_not_of(" \t");
    //     return (first == std::string::npos || last == std::string::npos) ? "" : str.substr(first, (last - first + 1));
    // }

    template <std::size_t N, typename... Args>
    struct StringTuple
    {
        using type = typename StringTuple<N - 1, std::string, Args...>::type;
    };

    template <typename... Args>
    struct StringTuple<0, Args...>
    {
        using type = std::tuple<Args...>;
    };

    template <std::size_t N>
    using StrTuple = typename StringTuple<N>::type;

    // Recursive template to fill a tuple from regex matches
    template <typename Tuple, std::size_t Index, std::size_t... Is>
    inline auto fillTuple(const std::smatch &matches, std::index_sequence<Is...>)
    {
        return Tuple{matches[Is + 1].str()...}; // +1 to skip the full match
    }

    template <typename Tuple, std::size_t N>
    inline auto fillTuple(const std::smatch &matches)
    {
        return fillTuple<Tuple, N>(matches, std::make_index_sequence<N>{});
    }

    template <std::size_t N>
    inline StrTuple<N> getData(const std::string &line, const std::string &rx)
    {
        try
        {
            std::smatch matches;
            std::regex regex_pattern(rx);
            std::regex_search(line, matches, regex_pattern);
            if (matches.size() < N + 1)
            {
                throw std::runtime_error("Not enough matches found");
            }
            return fillTuple<StrTuple<N>, N>(matches);
        }
        catch (std::runtime_error e)
        {
            return {};
        }
    }

}

#endif