/*
   Copyright (C) 2024 Viacheslav Domnin

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/


#ifndef _FILEPARSER_H_
#define _FILEPARSER_H_

#include <vector>
#include <string>
#include <boost/interprocess/file_mapping.hpp>
#include <boost/interprocess/mapped_region.hpp>
#include <memory>
#include "FileParserHelper.h"

using namespace boost::interprocess;
class FileParser
{
public:
    FileParser(std::string filename);
    ~FileParser();
    std::string getLine(std::size_t position = std::string::npos);
    std::pair<std::string, std::size_t> getLineAndPosition(bool reset = false);
    std::size_t size();
    std::size_t curret();
    char *begin();
    char *last();
    void reset();

private:
    std::string filename_;
    std::unique_ptr<file_mapping> fileMap;
    std::unique_ptr<mapped_region> region;
    char *end = nullptr;
    char *lineStart = nullptr;
    char *first = nullptr;

};

#endif // _FILEPARSER_H_
