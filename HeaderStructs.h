
#ifndef _HEADER_STRUCTS_H_
#define _HEADER_STRUCTS_H_

#include <string>
#include <tuple>

using StrTuple2 = std::tuple<std::string, std::string>;
using StrTuple3 = std::tuple<std::string, std::string, std::string>;
using StrTuple4 = std::tuple<std::string, std::string, std::string, std::string>;

struct MainInfo {
    std::string organization;
    std::string app;
    std::string version;
    MainInfo(){};
    MainInfo(const StrTuple3& tuple)
    : organization(std::move(std::get<0>(tuple))),
      app(std::move(std::get<1>(tuple))),
      version(std::move(std::get<2>(tuple))) {}
};

struct MeasureType {
    std::string type;
    std::string value;
    MeasureType(){};
    MeasureType(const StrTuple2& tuple)
    : type(std::move(std::get<0>(tuple))),
      value(std::move(std::get<1>(tuple))) {}
};

struct TimeAndSignal {
    std::string svalue;
    std::string unit;
    std::string signal;
    TimeAndSignal(){};
    TimeAndSignal(const StrTuple3& tuple)
    : svalue(std::move(std::get<0>(tuple))),
      unit(std::move(std::get<1>(tuple))),
      signal(std::move(std::get<2>(tuple))) {}
};

struct InputData {
    std::string inputType;
    std::string parameters;
    std::string filterCommon;
    std::string status;
    InputData(){};
    InputData(const StrTuple4& tuple)
    : inputType(std::move(std::get<0>(tuple))),
      parameters(std::move(std::get<1>(tuple))),
      filterCommon(std::move(std::get<2>(tuple))),
      status(std::move(std::get<3>(tuple))) {}
};

struct ArmOscData {
    std::string arm;
    std::string osc;
    ArmOscData(){};
    ArmOscData(const StrTuple2& tuple)
    : arm(std::move(std::get<0>(tuple))),
      osc(std::move(std::get<1>(tuple))) {}
};

struct HoldStatisticsData {
    std::string hold;
    std::string statistics;
    HoldStatisticsData(){};
    HoldStatisticsData(const StrTuple2& tuple)
    : hold(std::move(std::get<0>(tuple))),
      statistics(std::move(std::get<1>(tuple))) {}
};


#endif