#include "plotwidget2.h"

MyOpenGLRenderer::MyOpenGLRenderer(){
    initializeOpenGLFunctions();
    isInitialized = true;
    connect(this, &MyOpenGLRenderer::dataLoaded, this, &MyOpenGLRenderer::pointsLoaded, Qt::QueuedConnection);
}


void MyOpenGLRenderer::render() {
    if (!isInitialized) {
        initializeOpenGLFunctions();
        isInitialized = true;
    }
    auto start = std::chrono::high_resolution_clock::now();
    auto end = std::chrono::high_resolution_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();
    glClearColor(0, 0, 0, 1);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(maxmin_.minX, maxmin_.maxX, maxmin_.minY, maxmin_.maxY, -1, 1);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glColor3f(1.0f, 0.0f, 0.0f);  // Red
    glPointSize(5.0f);
    glBegin(GL_POINTS);
    for (const QPointF& point : dataPoints) {
        glVertex2f(point.x(), point.y());
    }
    glEnd();



    glColor3f(0.0f, 0.0f, 1.0f);  // Blue
    glLineWidth(2.0f);
    glBegin(GL_LINE_STRIP);
    for (const QPointF& point : dataPoints) {
        glVertex2f(point.x(), point.y());
    }

    end = std::chrono::high_resolution_clock::now();
    duration = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();
    std::cout << "TEST ____ RENDER: " << duration << " ms" << std::endl;

    glEnd();
}
