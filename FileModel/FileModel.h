/*
   Copyright (C) 2024 Viacheslav Domnin

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/


#ifndef FILEMODEL_FILEMODE_H_
#define FILEMODEL_FILEMODE_H_

#include <cstring>
#include <ctime>
#include <memory>
#include "../HeaderStructs.h"
#include "../FileParser/FileParser.h"
#include <unordered_set>
#include <list>
#include <map>

struct MaxMin {
    double maxX, maxY, minX, minY;
};



void printM( class FileModel &obj);
void durationTest(FileModel &obj);
void durationTest2(FileModel &obj);
class FileModel
{
public:
    FileModel(std::string file);

    std::tuple<std::map<double, double>, MaxMin>loadData(std::size_t step = 1);
    std::tuple<std::map<double, double>, MaxMin>loadData2();
    std::size_t size();
    std::tuple<MainInfo, MeasureType, std::time_t, TimeAndSignal, InputData, InputData, ArmOscData, HoldStatisticsData> readHeader();

    friend void printM( class FileModel &obj);
    friend void durationTest(FileModel &obj);
    friend void durationTest2(FileModel &obj);
private:
    std::size_t dataFirstLine(bool reset = false);


private:
    MainInfo mainInfo;
    MeasureType measureType;
    TimeAndSignal timeAndSignal;
    InputData inputDataA;
    InputData inputDataB;
    ArmOscData armOsc;
    HoldStatisticsData holdStatistics;
    std::time_t time_;
    std::unique_ptr<FileParser> fileParser;

    std::size_t dataStartPoint;
    std::size_t dataEndPoint;
    std::size_t currentPosition;
};

#endif // FILEMODEL_FILEMODE_H_
