/*
   Copyright (C) 2024 Viacheslav Domnin

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License
   as published by the Free Software Foundation; either version 2
   of the License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/


#include "FileModel.h"
#include "FileParser/FileParser.h"
#include <iostream>
#include <future>
#include <functional> 
#include <limits>
#include <future>
#include <thread>
#include <mutex>
#include <map>
#include <tuple>
#include <vector>
#include <iostream>


std::string MainInfoRgx = R"(^([^,]+),\s*(\w+)\s*V([\d\.]+))";
std::string MeasureTypeRgx = R"(^\#\s*(FREQUENCY)\s+(A))";
std::string DateAndTimeRgx = R"(^\#\s+\w+\s+(\w+)\s+(\d{1,2})\s+(\d{2}):(\d{2}):(\d{2})\s+(\d{4}))";
std::string TimeAndSignaRgx = R"(Measuring time:\s+(\d+)\s+(\w+)\s+Single:\s+(\w+))";
std::string InputRgx = R"(^\#\s+(Input [AB]):\s+([^F]+)\s+(Filter|Common):\s+(\w+))";
std::string ArmAndOsc = R"(Ext\.arm:\s+(\w+)\s+Ref\.osc:\s+(\w+))";
std::string HoldStatisticRgx = R"(Hold off:\s+(\w+)\s+Statistics:\s+(\w+))";
std::string DataDetectLine = R"((-?\d+\.\d+e[+-]\d+)\s+(-?\d+\.\d+e[+-]\d+))";

auto convertTime(const fp::StrTuple<6> &dateStr)
{
    std::tm tm = {};
    std::string date_str = std::get<0>(dateStr) + " " + std::get<1>(dateStr) + " " + std::get<5>(dateStr) +
                           " " + std::get<2>(dateStr) + ":" + std::get<3>(dateStr) + ":" + std::get<4>(dateStr);
    std::istringstream ss(date_str);
    ss >> std::get_time(&tm, "%b %d %Y %H:%M:%S");
    std::time_t time = std::mktime(&tm);
    return time;
}

std::tuple<MainInfo, MeasureType, std::time_t, TimeAndSignal, InputData, InputData, ArmOscData, HoldStatisticsData> FileModel::readHeader()
{
    mainInfo = fp::getData<3>(fileParser->getLine(0), MainInfoRgx);
    measureType = fp::getData<2>(fileParser->getLine(), MeasureTypeRgx);
    time_ = convertTime(fp::getData<6>(fileParser->getLine(), DateAndTimeRgx));
    timeAndSignal = fp::getData<3>(fileParser->getLine(), TimeAndSignaRgx);
    inputDataA = fp::getData<4>(fileParser->getLine(), InputRgx);
    inputDataB = fp::getData<4>(fileParser->getLine(), InputRgx);
    armOsc = fp::getData<2>(fileParser->getLine(), ArmAndOsc);
    holdStatistics = fp::getData<2>(fileParser->getLine(), HoldStatisticRgx);
    return std::make_tuple(mainInfo, measureType, time_, timeAndSignal, inputDataA, inputDataB, armOsc, holdStatistics);
}

std::size_t FileModel::dataFirstLine(bool reset)
{
    std::size_t dataStartPoint = 0;
    auto lineInfo = fileParser->getLineAndPosition(reset);
    for (; !lineInfo.first.empty();)
    {
        auto [tstmp, value] = fp::getData<2>(lineInfo.first, DataDetectLine);
        if (!tstmp.empty() && !value.empty())
        {
            dataStartPoint = lineInfo.second;
            break;
        }
        lineInfo = fileParser->getLineAndPosition();
    }
    return dataStartPoint;
}

std::size_t FileModel::size() {
    auto first = dataFirstLine(true);
    auto second = dataFirstLine();
    auto lineStep = second - first;
    auto fileSize = fileParser->size();
    auto dataSize = fileSize - first;
    auto estimated_size = dataSize / lineStep;
    return estimated_size;
}


std::map<double, double> processChunk(std::size_t startPos, std::size_t fileSize, std::size_t lineStep, std::size_t step, FileParser* fileParser,
                                     double& localMinX, double& localMaxX, double& localMinY, double& localMaxY) {
    std::map<double, double> localValues;
    std::istringstream istream;

    localMinX = std::numeric_limits<double>::max();
    localMaxX = std::numeric_limits<double>::lowest();
    localMinY = std::numeric_limits<double>::max();
    localMaxY = std::numeric_limits<double>::lowest();

    for (std::size_t position = startPos; position < fileSize; position += lineStep * step) {
        auto line = fileParser->getLine(position);
        istream.clear();
        istream.str(line);

        double num1, num2;
        if (istream >> num1 >> num2) {
            localValues[num1] = num2;

            if (num1 > localMaxX) localMaxX = num1;
            if (num1 < localMinX) localMinX = num1;
            if (num2 > localMaxY) localMaxY = num2;
            if (num2 < localMinY) localMinY = num2;
        }
    }

    return localValues;
}

std::tuple<std::map<double, double>, MaxMin> FileModel::loadData(std::size_t step) {
    auto first = dataFirstLine(true);
    auto second = dataFirstLine();
    auto lineStep = second - first;
    auto fileSize = fileParser->size();
    auto dataSize = fileParser->size() - first;

    std::size_t numThreads = std::thread::hardware_concurrency();
    std::vector<std::future<std::map<double, double>>> futures;

    std::mutex mtx;
    double minX = std::numeric_limits<double>::max(), maxX = std::numeric_limits<double>::lowest();
    double minY = std::numeric_limits<double>::max(), maxY = std::numeric_limits<double>::lowest();
    std::map<double, double> values;

    for (std::size_t i = 0; i < numThreads; ++i) {
        auto startPos = first + i * lineStep * step;

        futures.push_back(std::async(std::launch::async, [startPos, fileSize, lineStep, step, this](double& localMinX, double& localMaxX, double& localMinY, double& localMaxY) {
            return processChunk(startPos, fileSize, lineStep, step, this->fileParser.get(), localMinX, localMaxX, localMinY, localMaxY);
        }, std::ref(minX), std::ref(maxX), std::ref(minY), std::ref(maxY)));
    }

    for (auto& future : futures) {
        auto localValues = future.get();
        std::lock_guard<std::mutex> lock(mtx);
        for (const auto& [key, value] : localValues) {
            values[key] = value;
        }
    }

    return {values, {maxX, maxY, minX, minY}};
}
    
std::tuple<std::map<double, double>, MaxMin> FileModel::loadData2()
{
    auto first = dataFirstLine(true);
    std::istringstream istream;
    istream.str(fileParser->begin() + first);
    std::map<double, double> values;
    // values.reserve(estimated_size + 10);
    auto counter = 0u;

    double minX = std::numeric_limits<double>::max(), maxX = std::numeric_limits<double>::lowest();
    double minY = std::numeric_limits<double>::max(), maxY = std::numeric_limits<double>::lowest();
    
    double num1, num2;
    while (istream >> num1)
    {

        if (istream >> num2) {

        }
        values[num1]=num2;

        if (num1 > maxX) maxX = num1;
        if (num1 < minX) minX = num1;
        if (num2 > maxY) maxY = num2;
        if (num2 < minY) minY = num2;

            counter++;
    }
    return {values, {maxX, maxY, minX, minY}};
}

FileModel::FileModel(std::string file)
{
    fileParser = std::make_unique<FileParser>(file);
    // readHeader();

    // loadData(1);
    // durationTest2(*this);
    // fileParser->reset();
    // fileParser = std::make_unique<FileParser>(file);
    // durationTest(*this);
    // printM(*this);
}

void durationTest2(FileModel &obj)
{
    auto start = std::chrono::high_resolution_clock::now();
    auto end = std::chrono::high_resolution_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();
    int conuterlines = 0;

    obj.fileParser->reset();
    start = std::chrono::high_resolution_clock::now();
    obj.loadData2();
    end = std::chrono::high_resolution_clock::now();
    duration = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();
    std::cout << "TEST ____ Duration just usual istringstream: " << duration << " ms" << " counter: " << conuterlines << std::endl;
}


void durationTest(FileModel &obj)
{
    auto start = std::chrono::high_resolution_clock::now();
    auto end = std::chrono::high_resolution_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();
    int conuterlines = 0;

    obj.fileParser->reset();
    start = std::chrono::high_resolution_clock::now();
    obj.loadData(100);
    end = std::chrono::high_resolution_clock::now();
    duration = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();
    std::cout << "TEST ____ Duration5: " << duration << " ms" << " counter: " << conuterlines << std::endl;

    // obj.fileParser->reset();
    // start = std::chrono::high_resolution_clock::now();
    // obj.loadData(10);
    // end = std::chrono::high_resolution_clock::now();
    // duration = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();
    // std::cout << "TEST ____ Duration6: " << duration << " ms" << " counter: " << conuterlines << std::endl;

    obj.fileParser->reset();
    start = std::chrono::high_resolution_clock::now();
    obj.loadData(1);
    end = std::chrono::high_resolution_clock::now();
    duration = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();
    std::cout << "TEST ____ Duration mine: " << duration << " ms" << " counter: " << conuterlines << std::endl;
}


void printM(FileModel &obj)
{
    std::cout << "Main Information:" << std::endl
              << "  Organization: " << obj.mainInfo.organization << std::endl
              << "  App: " << obj.mainInfo.app << std::endl
              << "  Version: " << obj.mainInfo.version << std::endl;

    std::cout << "Measure Type:" << std::endl
              << "  Type: " << obj.measureType.type << std::endl
              << "  Value: " << obj.measureType.value << std::endl;
    std::tm *timeStruct = std::gmtime(&obj.time_);
    char buffer[80];
    std::strftime(buffer, sizeof(buffer), "%Y-%m-%d %H:%M:%S", timeStruct);
    std::cout << "Current time: "
              << buffer << std::endl;

    std::cout << "Time and Signal:" << std::endl
              << "  Svalue: " << obj.timeAndSignal.svalue << std::endl
              << "  Unit: " << obj.timeAndSignal.unit << std::endl
              << "  Signal: " << obj.timeAndSignal.signal << std::endl;

    std::cout << "Input Data A:" << std::endl
              << "  Input Type: " << obj.inputDataA.inputType << std::endl
              << "  Parameters: " << obj.inputDataA.parameters << std::endl
              << "  Filter/Common: " << obj.inputDataA.filterCommon << std::endl
              << "  Status: " << obj.inputDataA.status << std::endl;

    std::cout << "Input Data B:" << std::endl
              << "  Input Type: " << obj.inputDataB.inputType << std::endl
              << "  Parameters: " << obj.inputDataB.parameters << std::endl
              << "  Filter/Common: " << obj.inputDataB.filterCommon << std::endl
              << "  Status: " << obj.inputDataB.status << std::endl;

    std::cout << "ARM and OSC Data:" << std::endl
              << "  ARM: " << obj.armOsc.arm << std::endl
              << "  OSC: " << obj.armOsc.osc << std::endl;

    std::cout << "Hold Statistics:" << std::endl
              << "  Hold: " << obj.holdStatistics.hold << std::endl
              << "  Statistics: " << obj.holdStatistics.statistics << std::endl;
}