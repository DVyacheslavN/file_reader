import QtQuick 2.15
import QtQuick.Controls 2.15
import Qt.labs.platform 1.0

Item {
    id: main_item
    property string filePath: ""
    height: bt.height

    FileDialog {
        id: fileDialog
        title: "Choose a File"
        nameFilters: ["SSD files (*.ssd)","RSD files (*.rsd)"]

        onAccepted: {
            main_item.filePath = fileDialog.currentFile.toString().replace("file://", "");
            console.log("Selected URL file path:", fileDialog.currentFile.toString().replace("file://", ""));
        }
    }
    Button {
        id: bt
        text: "Open File Dialog"
        anchors.centerIn: parent
        onClicked: {
            fileDialog.open()
        }
    }
}
