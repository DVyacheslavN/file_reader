import QtQuick 2.15
import QtQuick.Controls 2.15
import CustomComponents 1.0
import QtQuick.Layouts 1.15

import MyModule 1.0

ApplicationWindow {
    visible: true
    width: 400
    height: 800
    title: "Simple QML Application"


    Column {
            width: parent.width
            // height: 100

        spacing: 10
        topPadding: 30

        InfoComponent {
            spacing: 10
            padding: 20
            width: parent.width
            height: 220
            fileInfo: plot.fileInfo
        }

        MyComponent {
            id: fileSelector
            width:  parent.width
            anchors.horizontalCenter: parent.horizontalCenter
            onFilePathChanged: {
                console.log("PATH CNAGED")
            }
        }

        Text {
            text: "   Selected File: " + fileSelector.filePath
        }

        PlotWidget2 {
            id: plot
            filePath: fileSelector.filePath
            anchors.horizontalCenter: parent.horizontalCenter
            width: parent.width - 20
            height: 500
        }
    }

}
