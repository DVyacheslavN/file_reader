import QtQuick 2.15
import QtQuick.Controls 2.15


Column {
    property var fileInfo: []

    Row {
        width: parent.width
        Text {
            height: 20
            width: parent.width /3
            text: fileInfo.length > 0 ? fileInfo[0] : "--"
        }
        Text {
            height: 20
            width: parent.width /3
            text: fileInfo.length > 0 ? fileInfo[1] : "--"
        }
        Text {
            height: 20
            width: parent.width /3
            text: fileInfo.length > 0 ? fileInfo[2] : "--"
        }
    }
    Row {
        width: parent.width
        Text {
            height: 20
            width: parent.width /3
            text: fileInfo.length > 0 ? fileInfo[3] : "--"
        }
        Text {
            height: 20
            width: parent.width /3
            text: fileInfo.length > 0 ? fileInfo[4] : "--"
        }
    }
    Row {
        width: parent.width
        Text {
            height: 20
            width: parent.width /3
            text: fileInfo.length > 0 ? fileInfo[6] : "--"
        }
        Text {
            height: 20
            width: parent.width /3
            text: fileInfo.length > 0 ? fileInfo[7] : "--"
        }
        Text {
            height: 20
            width: parent.width /3
            text: fileInfo.length > 0 ? fileInfo[8] : "--"
        }
    }

    Row {
        width: parent.width
        Row {
            height: 20
            width: parent.width /3
            spacing: 10
            Text {
                height: 20
                width: contentWidth
                text: fileInfo.length > 0 ? fileInfo[9] : "--"
            }
            Text {
                height: 20

                width: contentWidth
                text: fileInfo.length > 0 ? fileInfo[10] : "--"
            }
        }
        Row {
            height: 20
            width: parent.width /3
            spacing: 10

            Text {
                height: 20
                width: contentWidth
                text: fileInfo.length > 0 ? fileInfo[11] : "--"
            }
            Text {
                height: 20
                width: contentWidth
                text: fileInfo.length > 0 ? fileInfo[12] : "--"
            }
        }
    }

    Row {
        width: parent.width
        Row {
            height: 20
            width: parent.width /3
            spacing: 10
            Text {
                height: 20
                width: contentWidth
                text: fileInfo.length > 0 ? fileInfo[13] : "--"
            }
            Text {
                height: 20

                width: contentWidth
                text: fileInfo.length > 0 ? fileInfo[14] : "--"
            }
        }
        Row {
            height: 20
            width: parent.width /3
            spacing: 10

            Text {
                height: 20
                width: contentWidth
                text: fileInfo.length > 0 ? fileInfo[15] : "--"
            }
            Text {
                height: 20
                width: contentWidth
                text: fileInfo.length > 0 ? fileInfo[16] : "--"
            }
        }
    }

    Row {
        width: parent.width
        Text {
            height: 20
            width: parent.width /3
            text: "Ext.arm:" + (fileInfo.length > 0 ? fileInfo[17] : "--")
        }
        Text {
            height: 20
            width: parent.width /3
            text: "Ref.osc:" + (fileInfo.length > 0 ? fileInfo[18] : "--")
        }
    }

    Row {
        width: parent.width
        Text {
            height: 20
            width: parent.width /3
            text: "Hold off:" + (fileInfo.length > 0 ? fileInfo[19] : "--")
        }
        Text {
            height: 20
            width: parent.width /3
            text: "Statistics:" + (fileInfo.length > 0 ? fileInfo[20] : "--")
        }
    }
}
