#include <QOpenGLFunctions>
#include <QQuickFramebufferObject>
#include <QOpenGLFramebufferObject>
#include <QOpenGLFramebufferObjectFormat>
#include <QQuickFramebufferObject>
#include <QtConcurrent>
#include <QFuture>
#include <QFutureWatcher>
#include <QDateTime>
#include "FileModel/FileModel.h"

class MyOpenGLRenderer : public QObject, public QQuickFramebufferObject::Renderer, protected QOpenGLFunctions {
    Q_OBJECT
public:
    MyOpenGLRenderer();

    void render() override;

    QOpenGLFramebufferObject *createFramebufferObject(const QSize &size) override {
        QOpenGLFramebufferObjectFormat format;
        format.setAttachment(QOpenGLFramebufferObject::CombinedDepthStencil);
        return new QOpenGLFramebufferObject(size, format);
    }

public slots:
    void filePathChanged(const QString &filePath) {
        QFuture<void> future = QtConcurrent::run([=]() {
            fileModel = std::make_unique<FileModel>(filePath.toStdString());

            auto [mainInfo, measureType, time_, timeAndSignal, inputDataA, inputDataB, armOsc, holdStatistics] = fileModel->readHeader();
            QStringList list{mainInfo.organization.c_str(), mainInfo.app.c_str(), mainInfo.version.c_str(),
                measureType.type.c_str(), measureType.value.c_str(),
                QDateTime::fromSecsSinceEpoch(time_).toString(),
                timeAndSignal.signal.c_str(), timeAndSignal.svalue.c_str(), timeAndSignal.unit.c_str(),
                inputDataA.inputType.c_str(), inputDataA.parameters.c_str(), inputDataA.filterCommon.c_str(), inputDataA.status.c_str(),
                inputDataB.inputType.c_str(), inputDataB.parameters.c_str(), inputDataB.filterCommon.c_str(), inputDataB.status.c_str(),
                armOsc.arm.c_str(), armOsc.osc.c_str(),
                holdStatistics.hold.c_str(), holdStatistics.statistics.c_str()
            };
            emit headerLoaded(list);
            if (fileModel->size() > 10000)
            {
                auto [data, maxmin] = fileModel->loadData(100);
                maxmin_ = maxmin;
                dataPoints.clear();
                    auto start = std::chrono::high_resolution_clock::now();
                    auto end = std::chrono::high_resolution_clock::now();
                    auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();
                for (const auto& pair : data) {
                    dataPoints.append(QPointF(pair.first, pair.second));
                }
                    end = std::chrono::high_resolution_clock::now();
                    duration = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();
                    qDebug() << "TEST ____ FOR: " << duration << " ms";
                emit dataLoaded(dataPoints, maxmin);
            }

            auto [data, maxmin] = fileModel->loadData(1);
            maxmin_ = maxmin;
            dataPoints.clear();
            for (const auto& pair : data) {
                dataPoints.append(QPointF(pair.first, pair.second));
            }
            emit dataLoaded(dataPoints, maxmin);
        });

        QFutureWatcher<void>* watcher = new QFutureWatcher<void>();
        connect(watcher, &QFutureWatcher<void>::finished, this, [this, watcher]() {
            watcher->deleteLater();
            update();
            emit fileLoaded();
        });
        watcher->setFuture(future);
    }

    void pointsLoaded(const QVector<QPointF> & points, const MaxMin & range) {
        dataPoints.clear();
        dataPoints = std::move(points);
        maxmin_ = range;
        emit fileLoaded();
        update();
    }


signals:
    void fileLoaded();
    void dataLoaded(const QVector<QPointF> &, const MaxMin &);
    void headerLoaded(const QStringList & fileInfo);
private:
    QVector<QPointF> dataPoints;
    std::unique_ptr<FileModel> fileModel = nullptr;
    MaxMin maxmin_;

    bool isInitialized = false;
};


class MyFboQuickItem : public QQuickFramebufferObject {
    Q_OBJECT
    Q_PROPERTY(QString filePath READ filePath WRITE setFilePath NOTIFY filePathChanged)
    Q_PROPERTY(QStringList fileInfo READ fileInfo NOTIFY fileInfoChanged)
public:
    QString filePath() const { return m_filePath; }
    QStringList fileInfo() const { return m_fileInfo; }
    Renderer *createRenderer() const override {
        MyOpenGLRenderer *ptr = new MyOpenGLRenderer();
        QObject::connect(this, &MyFboQuickItem::filePathChanged, ptr, &MyOpenGLRenderer::filePathChanged);
        QObject::connect(this, &MyFboQuickItem::filePathChanged, &MyFboQuickItem::update);
        QObject::connect(ptr, &MyOpenGLRenderer::fileLoaded, this, &MyFboQuickItem::update);
        QObject::connect(ptr, &MyOpenGLRenderer::dataLoaded, this, &MyFboQuickItem::update);
        QObject::connect(ptr, &MyOpenGLRenderer::headerLoaded, this, &MyFboQuickItem::setFileInfo);
        return ptr;
    }
    void setFilePath(const QString &filePath) {
        if (m_filePath != filePath) {
            m_filePath = filePath;
            emit filePathChanged(filePath);
        }
    }

    void setFileInfo(const QStringList &fileInfo) {
        m_fileInfo.clear();
        m_fileInfo += fileInfo;
        emit fileInfoChanged(m_fileInfo);
        update();
    }


    QString m_filePath;
    QStringList m_fileInfo;
signals:
    void filePathChanged(const QString &filePath);
    void fileInfoChanged(const QStringList &fileInfo);

private:
};
