cmake_minimum_required(VERSION 3.0.0)
project(file_viewer VERSION 0.1.0 LANGUAGES C CXX)
include(qt6-config.cmake)
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(THREADS_PREFER_PTHREAD_FLAG ON)
set(CMAKE_BUILD_TYPE Debug)

find_package(Threads REQUIRED)
find_package(Qt6 COMPONENTS Core Quick Qml Gui Widgets Concurrent OpenGL REQUIRED)
qt_standard_project_setup()

include(CTest)
enable_testing()

set(HEADERS FileParser/FileParser.h
    FileModel/FileModel.h
    plotwidge.h
    plotwidget2.h
)
set(SOURCES FileParser/FileParser.cpp
    FileModel/FileModel.cpp
    plotwidge.cpp
    plotwidget2.cpp
)

qt_add_executable(file_viewer
    main.cpp
    ${HEADERS}
    ${SOURCES}
)

qt_add_qml_module(file_viewer
    URI MyModule
    VERSION 1.0
    RESOURCE_PREFIX /imports
    QML_FILES
        qml_files/main.qml
        qml_files/MyComponent.qml
        qml_files/InfoComponent.qml
)

target_include_directories(file_viewer PRIVATE ${Qt6Core_INCLUDE_DIRS} ${Qt6Gui_INCLUDE_DIRS})
target_link_libraries(file_viewer PRIVATE Qt6::Core Qt6::Quick Qt6::Qml Qt6::Widgets Qt6::Gui Qt6::OpenGL Qt6::Concurrent)
set(CPACK_PROJECT_NAME ${PROJECT_NAME})
set(CPACK_PROJECT_VERSION ${PROJECT_VERSION})
include(CPack)
target_link_libraries(${PROJECT_NAME} PRIVATE Threads::Threads)
